package main

import (
	"context"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/jackc/pgx/v5"
	"github.com/nats-io/stan.go"
	"github.com/sirupsen/logrus"
	caches "gitlab.com/vresh2013/wb_l0/pkg/caches"
	"gitlab.com/vresh2013/wb_l0/pkg/handlers"
)

func main() {
	lg := logrus.New()
	lg.SetFormatter(&logrus.JSONFormatter{})
	lg.SetReportCaller(true)

	url := "postgres://myuser:mypassword@localhost:5432/wb_l0"
	conn, err := pgx.Connect(context.Background(), url)
	if err != nil {
		printErr(lg, err, "cannot connect")
	}

	defer func() {
		err := conn.Close(context.Background())
		if err != nil {
			printErr(lg, err, "cannot close")
		}
	}()

	err = conn.Ping(context.Background())
	if err != nil {
		printErr(lg, err, "cannot ping")
	}

	repo := items.RepoItem{
		DB: conn,
	}

	cache := caches.CreateCache(repo)

	handler := handlers.NewHandler()

	sc, err := stan.Connect("clusterID123", "clientID",
		stan.Pings(10, 5),
		stan.NatsURL("nats://localhost:4222"),
	)
	if err != nil {
		printErr(lg, err, "cannot connect to nats stream")
	}

	r := mux.NewRouter()

	r.HandleFunc("/{id}", handler.Main).Methods(http.MethodGet)

	port := ":8080"
	lg.WithFields(logrus.Fields{
		"port": port,
	}).Info("starting server")

	err = http.ListenAndServe(port, r)
	if err != nil {
		printErr(lg, err, "cannot select")
	}
}

func printErr(logger *logrus.Logger, err error, message string) {
	logger.WithFields(logrus.Fields{
		"error": err,
	}).Error(message)
}
