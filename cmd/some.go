package main

import (
	// "database/sql"

	"fmt"

	"github.com/sirupsen/logrus"

	// _ "github.com/lib/pq"

	stan "github.com/nats-io/stan.go"
)

// const (
// 	host     = "localhost"
// 	port     = 5432
// 	user     = "myuser"
// 	password = "mypassword"
// 	dbname   = "wb_l0"
// )

func main() {
	lg := logrus.New()
	lg.SetFormatter(&logrus.JSONFormatter{})
	lg.SetReportCaller(true)

	// url := "postgres://myuser:mypassword@localhost:5432/wb_l0"
	// conn, err := pgx.Connect(context.Background(), url)
	// if err != nil {
	// 	printErr(lg, err, "cannot connect")
	// }

	// defer func() {
	// 	err := conn.Close(context.Background())
	// 	if err != nil {
	// 		printErr(lg, err, "cannot close")
	// 	}
	// }()

	// err = conn.Ping(context.Background())
	// if err != nil {
	// 	printErr(lg, err, "cannot ping")
	// }

	// _, err = conn.Exec(context.Background(), "INSERT INTO payments (transactn, request_id) VALUES ($1, $2)", "11", "22")
	// if err != nil {
	// 	printErr(lg, err, "cannot exec")
	// }

	// id := -1
	// tr, rq := "", ""
	// err = conn.QueryRow(context.Background(), "SELECT id, transactn, request_id FROM payments").Scan(&id, &tr, &rq)
	// if err != nil {
	// 	printErr(lg, err, "cannot select")
	// }

	// fmt.Printf("id = %v, tr = %v, rq = %v\n", id, tr, rq)

	sc, err := stan.Connect("clusterID123", "clientID",
		stan.Pings(10, 5),
		stan.NatsURL("nats://localhost:4222"),
	)
	if err != nil {
		printErr(lg, err, "cannot select")
	}

	// Simple Synchronous Publisher
	sc.Publish("foo", []byte("Hello World"))

	// Simple Async Subscriber
	sub, _ := sc.Subscribe("foo", func(m *stan.Msg) {
		fmt.Printf("Received a message: %s\n", string(m.Data))
	})

	// Unsubscribe
	sub.Unsubscribe()

	fmt.Println("STARTED")

	// runtime.Goexit()
}

func printErr(logger *logrus.Logger, err error, message string) {
	logger.WithFields(logrus.Fields{
		"error": err,
	}).Error(message)
}
