SET NAMES 'utf8';

-- test table
-- DROP TABLE IF EXISTS test;
-- CREATE TABLE test (
--     id SERIAL,
--     transactn varchar(255) NOT NULL,
--     request_id varchar(255) NOT NULL,
--     PRIMARY KEY (id)
-- );


DROP TABLE IF EXISTS payments;
CREATE TABLE payments (
    id SERIAL,
    payment_transaction varchar(255) NOT NULL,
    request_id varchar(255) NOT NULL,
    currency varchar(255) NOT NULL,
    payment_provider varchar(255) NOT NULL,
    amount int NOT NULL,
    payment_dt bigint NOT NULL,
    bank varchar(255) NOT NULL,
    delivery_cost int NOT NULL,
    goods_total int NOT NULL,
    custom_fee int NOT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS delivery;
CREATE TABLE delivery (
    id SERIAL,
    cust_name varchar(255) NOT NULL,
    phone varchar(255) NOT NULL,
    zip varchar(255) NOT NULL,
    city varchar(255) NOT NULL,
    customer_address varchar(255) NOT NULL,
    region varchar(255) NOT NULL,
    email varchar(255) NOT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS items;
CREATE TABLE items (
    id SERIAL,
    chrt_id bigint NOT NULL,
    track_number varchar(255) NOT NULL,
    price int NOT NULL,
    rid varchar(255) NOT NULL,
    item_name varchar(255) NOT NULL,
    sale int NOT NULL,
    item_size varchar(255) NOT NULL,
    total_price int NOT NULL,
    nm_id bigint NOT NULL,
    brand varchar(255) NOT NULL,
    item_status int NOT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS orders;
CREATE TABLE orders (
    id SERIAL,
    order_uid varchar(255) NOT NULL,
    track_number varchar(255) NOT NULL,
    order_entry varchar(255) NOT NULL,
    CONSTRAINT fk_delivery
        FOREIGN KEY (id)
            REFERENCES delivery(id),
    locale  varchar(255) NOT NULL,
    internal_signature varchar(255) NOT NULL,
    customer_id varchar(255) NOT NULL,
    delivery_service varchar(255) NOT NULL,
    shardkey varchar(255) NOT NULL,
    sm_id int NOT NULL,
    date_created varchar(255) NOT NULL,
    oof_shard int NOT NULL,
    PRIMARY KEY (id)
);
