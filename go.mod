module gitlab.com/vresh2013/wb_l0

go 1.18

require (
	github.com/gorilla/mux v1.8.0
	github.com/jackc/pgx/v5 v5.4.3
	github.com/nats-io/stan.go v0.10.4
	github.com/sirupsen/logrus v1.9.3
)

require (
	github.com/gogo/protobuf v1.3.2 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20221227161230-091c0ba34f0a // indirect
	github.com/klauspost/compress v1.17.0 // indirect
	github.com/nats-io/nats-server/v2 v2.10.3 // indirect
	github.com/nats-io/nats-streaming-server v0.25.5 // indirect
	github.com/nats-io/nats.go v1.30.2 // indirect
	github.com/nats-io/nkeys v0.4.5 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	golang.org/x/crypto v0.13.0 // indirect
	golang.org/x/sys v0.12.0 // indirect
	golang.org/x/text v0.13.0 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
)
