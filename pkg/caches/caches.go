package cache

import (
	"sync"
	"time"

	"gitlab.com/vresh2013/wb_l0/pkg/items"
)

type Cache struct {
	Repo  items.Repo
	mu    *sync.Mutex
	cache map[string]*items.Order
}

func CreateCache(repo items.Repo) *Cache {
	return &Cache{
		Repo:  repo,
		mu:    &sync.Mutex{},
		cache: make(map[string]*items.Order),
	}
}

func (c *Cache) Start(refreshTime time.Duration) error {
	for {
		orders, err := c.Repo.All()
		if err != nil {
			return err
		}

		c.mu.Lock()

		c.cache = make(map[string]*items.Order)
		for _, order := range orders {
			c.cache[order.ID] = order
		}

		c.mu.Unlock()

		<-time.After(refreshTime)
	}
}

func (c *Cache) GetOrder(id string) *items.Order {
	c.mu.Lock()
	defer c.mu.Unlock()

	order, ok := c.cache[id]
	if !ok {
		return nil
	}

	return order
}
