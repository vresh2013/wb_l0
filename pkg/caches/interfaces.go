package cache

import (
	"time"

	"gitlab.com/vresh2013/wb_l0/pkg/items"
)

type CacheI interface {
	Start(refreshTime time.Duration) error
	GetOrder(id string) *items.Order
}
