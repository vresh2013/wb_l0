package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	caches "gitlab.com/vresh2013/wb_l0/pkg/caches"
)

type Handler struct {
	cache  caches.CacheI
	logger *logrus.Entry
}

func NewHandler(cache *caches.Cache, lg *logrus.Entry) *Handler {
	return &Handler{
		cache:  cache,
		logger: lg,
	}
}

func (h *Handler) Main(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	if vars == nil {
		h.logger.Error("nil mux vars")
		http.Error(w, "Internal server error", 500)
		return
	}

	id, ok := vars["id"]
	if !ok {
		h.logger.Error("no id in mux vars")
		http.Error(w, "Internal server error", 500)
		return
	}

	order := h.cache.GetOrder(id)
	if order == nil {
		http.Error(w, "wrong id", 400)
		return
	}

	js, err := json.Marshal(order)
	if err != nil {
		http.Error(w, "Internal server error", 500)
		printErr(h.logger, err, "cannot marshal")
		return
	}
	_, err = w.Write(js)
	if err != nil {
		printErr(h.logger, err, "cannot write")
		http.Error(w, "Internal server error", 500)
		return
	}
}

func printErr(logger *logrus.Entry, err error, message string) {
	logger.WithFields(logrus.Fields{
		"error": err,
	}).Error(message)
}
