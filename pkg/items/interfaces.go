package items

type Repo interface {
	All() ([]*Order, error)
}
