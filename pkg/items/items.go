package items

import "github.com/jackc/pgx"

type Payment struct {
	Payment_transaction string `json:"transaction"`
	Request_id          string
	Currency            string
	Payment_provider    string `json:"provider"`
	Amount              int
	Payment_dt          int64
	Bank                string
	Delivery_cost       int
	Goods_total         int
	Custom_fee          int
}

type Delivery struct {
	Cust_name        string `json:"name"`
	Phone            string
	Zip              string
	City             string
	Customer_address string `json:"address"`
	Region           string
	Email            string
}

type Item struct {
	Chrt_id      int64
	Track_number string
	Price        int
	Rid          string
	Item_name    string `json:"name"`
	Sale         int
	Item_size    string `json:"size"`
	Total_price  int
	Nm_id        int64
	Brand        string
	Item_status  int `json:"status"`
}

type Order struct {
	Order_uid          string
	Track_number       string
	Order_entry        string   `json:"entry"`
	Delivery           Delivery `json:"delivery"`
	Payment            Payment  `json:"payment"`
	Items              []Item
	Locale             string
	Internal_signature string
	Customer_id        string
	Delivery_service   string
	Shardkey           string
	Sm_id              int
	Date_created       string
	Oof_shard          int
}

type RepoOrder struct {
	DB *pgx.Conn
}

func (repo *RepoOrder) All() ([]*Order, error) {

}
